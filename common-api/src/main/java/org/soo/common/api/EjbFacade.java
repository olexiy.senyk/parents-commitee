package org.soo.common.api;

import javax.ejb.Remote;
import java.util.function.Function;

@Remote
public interface EjbFacade<T, R> extends Function<T, R> {
}
