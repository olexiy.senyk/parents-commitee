package org.soo.common.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.naming.InitialContext;
import javax.naming.NamingException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FacadeAccessor {

    public static <T, R> EjbFacade<T, R> getFacade(String name) throws NamingException {
        return (EjbFacade<T, R>) new InitialContext().lookup(name);
    }

    public static <T> T lookupService(String serviceName) throws NamingException {
        return (T) new InitialContext().lookup(serviceName);
    }
}
