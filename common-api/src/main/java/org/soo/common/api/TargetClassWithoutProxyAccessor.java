package org.soo.common.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.interceptor.InvocationContext;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TargetClassWithoutProxyAccessor {

    public static Class<?> get(InvocationContext invocationContext) {
        final Object targetInstance = invocationContext.getTarget();
        final Class<?> targetClass = targetInstance.getClass().getSuperclass(); // remove proxy

        return targetClass == Object.class
                ? targetInstance.getClass() // no proxy
                : targetClass;
    }
}
