package org.soo.parent.committee.middleware.user.api;

import javax.ejb.Remote;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Remote
public interface UserService {

    RegistrationResponse registration(@NotNull @Valid RegistrationRequest request);

    UserDto confirmation(@NotNull String token);

    UserDto findById(@NotNull Long id);

    UserDto findByEmail(@NotNull String email);
}
