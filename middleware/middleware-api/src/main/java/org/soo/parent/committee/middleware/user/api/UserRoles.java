package org.soo.parent.committee.middleware.user.api;

import java.util.Collections;
import java.util.Set;

public enum UserRoles {

    user,
    admin;

    public static Set<UserRoles> defaultUserRoles() {
        return Collections.singleton(user);
    }

    public static Set<UserRoles> defaultAdminRoles() {
        return Collections.singleton(admin);
    }
}
