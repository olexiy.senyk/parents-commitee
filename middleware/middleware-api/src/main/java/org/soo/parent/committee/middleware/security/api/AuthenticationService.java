package org.soo.parent.committee.middleware.security.api;

import javax.ejb.Remote;

@Remote
public interface AuthenticationService {

    AuthenticationResult authenticate(AuthenticationRequest request);
}
