package org.soo.parent.committee.middleware.security.api;

import lombok.*;
import org.soo.parent.committee.middleware.user.api.UserDto;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationResult implements Serializable {

    private static final long serialVersionUID = 5086656816569325002L;

    private boolean authenticated;
    private UserDto user;
}
