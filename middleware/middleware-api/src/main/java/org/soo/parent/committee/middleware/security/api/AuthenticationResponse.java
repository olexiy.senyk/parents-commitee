package org.soo.parent.committee.middleware.security.api;

import lombok.Value;

import java.io.Serializable;

@Value
public class AuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 7920818115819822199L;

    public boolean authenticated;
    public String token;

    public static AuthenticationResponse authenticated(String token) {
        return new AuthenticationResponse(true, token);
    }

    public static AuthenticationResponse notAuthenticated() {
        return new AuthenticationResponse(false, null);
    }
}
