package org.soo.parent.committee.middleware.user.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationResponse implements Serializable {

    // todo: java 14+ @Serial
    private static final long serialVersionUID = -5702295457416466391L;

    private boolean result;

    public static RegistrationResponse ok() {
        return new RegistrationResponse(true);
    }
}
