package org.soo.parent.committee.middleware.user.service;

import org.soo.parent.committee.middleware.user.api.*;
import org.soo.parent.committee.middleware.user.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import static javax.transaction.Transactional.TxType.REQUIRED;
import static javax.transaction.Transactional.TxType.SUPPORTS;

/**
 * <h3>EJB/JNDI names:</h3>
 * <ul>
 * <li>java:global/middleware/UserServiceImpl</li>
 * <li>java:global/middleware/UserServiceImpl!org.soo.parent.committee.middleware.user.api.UserService</li>
 * <ul/>
 */
@Stateless
@Transactional(SUPPORTS)
public class UserServiceImpl implements UserService {

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private Function<User, UserDto> userEntityToDtoMapper;

    @Inject
    private UserRepository repository;

    @Override
    @Transactional(REQUIRED)
    public RegistrationResponse registration(@NotNull @Valid RegistrationRequest request) {
        final User prototype = new User();
        prototype.setEmail(request.getEmail());
        prototype.setPassword(passwordEncoder.encode(request.getPassword()));
        prototype.setRoles(request.getRoles());
        repository.merge(prototype);

        return RegistrationResponse.ok();
    }

    @Override
    public UserDto confirmation(String token) {
        return new UserDto();
    }

    @Override
    public UserDto findById(Long id) {
        final User applicationUser = repository.findById(id);
        return userEntityToDtoMapper.apply(applicationUser);
    }

    @Override
    public UserDto findByEmail(String email) {
        final User applicationUser = repository.findByEmail(email);
        return userEntityToDtoMapper.apply(applicationUser);
    }
}
