package org.soo.parent.committee.middleware.security;

import org.soo.parent.committee.middleware.security.api.AuthenticationRequest;
import org.soo.parent.committee.middleware.security.api.AuthenticationResult;
import org.soo.parent.committee.middleware.security.api.AuthenticationService;
import org.soo.parent.committee.middleware.user.api.UserDto;
import org.soo.parent.committee.middleware.user.model.User;
import org.soo.parent.committee.middleware.user.service.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.function.Function;

/**
 * <h3>EJB/JNDI names:</h3>
 * <ul>
 * <li> java:global/middleware/AuthenticationServiceImpl</li>
 * <li> java:global/middleware/AuthenticationServiceImpl!org.soo.parent.committee.middleware.security.api.AuthenticationService</li>
 * </ul>
 */
@Stateless
public class AuthenticationServiceImpl implements AuthenticationService {

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository repository;

    @Inject
    private Function<User, UserDto> userEntityToDtoMapper;

    @Override
    public AuthenticationResult authenticate(AuthenticationRequest request) {
        final User user = repository.findByEmail(request.getEmail());
        final AuthenticationResult result = new AuthenticationResult();
        result.setAuthenticated(user != null && passwordEncoder.matches(request.getPassword(), user.getPassword()));

        if (result.isAuthenticated()) {
            result.setUser(userEntityToDtoMapper.apply(user));
        }

        return result;
    }
}
