package org.soo.parent.committee.middleware.security;

import org.springframework.security.crypto.password.PasswordEncoder;

public interface PasswordEncoderFactory {

    PasswordEncoder produce();
}
