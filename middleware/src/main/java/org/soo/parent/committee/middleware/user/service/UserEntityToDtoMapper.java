package org.soo.parent.committee.middleware.user.service;

import org.soo.parent.committee.middleware.user.api.UserDto;
import org.soo.parent.committee.middleware.user.model.User;

import javax.inject.Singleton;
import java.util.function.Function;

@Singleton
public class UserEntityToDtoMapper implements Function<User, UserDto> {

    @Override
    public UserDto apply(User user) {
        if (user == null) {
            return null;
        }

        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setRoles(user.getRoles());

        return userDto;
    }
}
