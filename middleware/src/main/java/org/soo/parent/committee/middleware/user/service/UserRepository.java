package org.soo.parent.committee.middleware.user.service;

import org.soo.parent.committee.middleware.user.model.User;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

@Named
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public User merge(@NotNull User prototype) {
        return em.merge(prototype);
    }

    public User findById(Long id) {
        return em.find(User.class, id);
    }

    public User findByEmail(String email) {
        return em.createNamedQuery("user.findByEmail", User.class)
                .setParameter("email", email)
                .getSingleResult();
    }
}
