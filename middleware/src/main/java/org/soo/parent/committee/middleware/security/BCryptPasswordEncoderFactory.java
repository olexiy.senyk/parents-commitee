package org.soo.parent.committee.middleware.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.enterprise.inject.Produces;

public class BCryptPasswordEncoderFactory implements PasswordEncoderFactory {

    @Override
    @Produces
    public PasswordEncoder produce() {
        return new BCryptPasswordEncoder();
    }
}
