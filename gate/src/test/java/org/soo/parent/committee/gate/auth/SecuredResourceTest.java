package org.soo.parent.committee.gate.auth;

import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.soo.parent.committee.middleware.security.api.AuthenticationRequest;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@Disabled
class SecuredResourceTest {

    private static final String BASE_URI = "http://localhost:8080/gate";
    private static final String AUTH_URI = BASE_URI.concat("/auth");
    private static final String SECURED_URI = BASE_URI.concat("/secured/");
    private static final String URI_USERNAME = "username";
    private static final String URI_USER = "user";
    private static final String URI_ADMIN = "admin";

    private static final String AUTHENTICATED = "authenticated";
    private static final String TOKEN = "token";

    private static final String USER = "user@local.mail.org";
    private static final String ADMIN = "admin@local.mail.org";
    private static final String USER_ADMIN = "user.admin@local.mail.org";
    private static final String PASSWORD = "password";

    @BeforeAll
    public static void init() {
        final ObjectMapperConfig objectMapperConfig = new ObjectMapperConfig();
        objectMapperConfig.defaultObjectMapper();
        RestAssured.config().objectMapperConfig(objectMapperConfig);
    }

    @Test
    void testAuthenticateUser() {
        authenticate(USER, PASSWORD);
    }

    @Test
    void testUsernameForUser() {
        final String token = authenticate(USER, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_USERNAME);
        assertEquals(USER, response);
    }

    @Test
    void testUsernameForAdmin() {
        final String token = authenticate(ADMIN, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_USERNAME);
        assertEquals(ADMIN, response);
    }

    @Test
    void testUsernameForUserAdmin() {
        final String token = authenticate(USER_ADMIN, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_USERNAME);
        assertEquals(USER_ADMIN, response);
    }

    @Test
    void testUserAllowed() {
        final String token = authenticate(USER, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_USER);
        assertEquals("You are logged with jwt-rbac: [user]", response);
    }

    @Test
    void testUserForbidden() {
        final String token = authenticate(USER, PASSWORD);
        RestAssured.baseURI = SECURED_URI;
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .auth()
                .preemptive().oauth2(token)
                .when().get(URI_ADMIN)
                .then().statusCode(HttpStatus.SC_FORBIDDEN);
    }

    @Test
    void testAdminAllowed() {
        final String token = authenticate(ADMIN, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_ADMIN);
        assertEquals("You are logged with jwt-rbac: [admin]", response);
    }

    @Test
    void testAdminForbidden() {
        final String token = authenticate(ADMIN, PASSWORD);
        RestAssured.baseURI = SECURED_URI;
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .auth()
                .preemptive().oauth2(token)
                .when().get(URI_USER)
                .then().statusCode(HttpStatus.SC_FORBIDDEN);
    }

    @Test
    void testUserAdminAllowedUser() {
        final String token = authenticate(USER_ADMIN, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_USER);
        assertEquals("You are logged with jwt-rbac: [admin, user]", response);
    }

    @Test
    void testUserAdminAllowedAdmin() {
        final String token = authenticate(USER_ADMIN, PASSWORD);
        final String response = getSecuredEndpoint(token, URI_ADMIN);
        assertEquals("You are logged with jwt-rbac: [admin, user]", response);
    }

    private String authenticate(String user, String password) {
        RestAssured.baseURI = AUTH_URI;
        final AuthenticationRequest request = new AuthenticationRequest();
        request.setEmail(user);
        request.setPassword(password);

        final Response response = given()
                .urlEncodingEnabled(true)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request)
                .when().post()
                .then().statusCode(HttpStatus.SC_OK)
                .extract().response();

        final Boolean authenticated = response.jsonPath().get(AUTHENTICATED);
        final String token = response.jsonPath().get(TOKEN);
        assertTrue(authenticated);
        assertNotNull(token);

        return token;
    }

    @Test
    private String getSecuredEndpoint(String token, String url) {
        RestAssured.baseURI = SECURED_URI;
        final Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .auth()
                .preemptive().oauth2(token)
                .when().get(url)
                .then().statusCode(HttpStatus.SC_OK)
                .extract().response();
        return response.getBody().prettyPrint();
    }
}
