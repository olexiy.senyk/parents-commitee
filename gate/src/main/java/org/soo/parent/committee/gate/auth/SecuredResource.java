package org.soo.parent.committee.gate.auth;

import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonString;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Optional;
import java.util.Set;

@Path("secured")
@RequestScoped
@DenyAll
@Produces("application/json")
@Consumes("application/json")
public class SecuredResource {

    @Inject
    @Claim(standard = Claims.groups)
    private Set<String> groups;

    @Inject
    @Claim(standard = Claims.sub)
    private String subject;

    @Inject
    @Claim("upn")
    private Optional<JsonString> upn;

    @GET
    @RolesAllowed({"user", "admin"})
    @Path("/username")
    public String getUsername() {
        return upn.isPresent() ? upn.get().getString() : "NULL";
    }

    @GET
    @Path("user")
    @RolesAllowed("user")
    public String user() {
        return composeMessage();
    }

    @GET
    @Path("admin")
    @RolesAllowed("admin")
    public String admin() {
        return composeMessage();
    }

    private String composeMessage() {
        return "You are logged with " + this.subject + ": " + this.groups.toString();
    }
}