package org.soo.parent.committee.gate.auth;

import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import org.soo.parent.committee.middleware.user.api.UserDto;
import org.soo.parent.committee.middleware.user.api.UserRoles;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class JwtUtils {

    /*
        https://www.eclipse.org/community/eclipse_newsletter/2017/september/article2.php
        Minimum MP-JWT Required Claims:
        todo: Inject claim properties with MicroProfile Config
              + typ     "JWT"       (smallrye:auto)
              + alg     "RS256"     (smallrye:auto)
              - kid     "META-INF/private_key.pem"
              - iss     "https://localhost"
              - sub     Identifies the principal that is the subject of the JWT. See the "upn" claim for how this
                        relates to the runtime java.security.Principal
                        RFC7519, Section 4.1.2
              - aud     Identifies the recipients that the JWT is intended for.
                        RFC7519, Section 4.1.3
              + exp     Identifies the expiration time on or after which the JWT MUST NOT be accepted for processing.
                        RFC7519, Section 4.1.4
              + iat     Identifies the time at which the issuer generated the JWT.
                        RFC7519, Section 4.1.6
              - jti     Provides a unique identifier for the JWT.
                        RFC7519, Section 4.1.7
              - upn     Provides the user principal name in the java.security.Principal interface.
                        MP-JWT 1.0 specification
              - groups  Provides the list of group names that have been assigned to the principal of the MP-JWT.
                        This typically will require a mapping at the application container level to application
                        deployment roles, but a one-to-one between group names and application role names is required
                        to be performed in addition to any other mapping.
                        MP-JWT 1.0 specification
     */

    private static final String JTI = "jti";

    private static final String iss = "https://localhost";
    private static final String sub = "jwt-rbac";
    private static final String aud = "todo";
    private static final String signatureKeyId = "META-INF/private_key.pem";

    public static String createJWT(UserDto user) {
        try {
            final Set<String> groups = user.getRoles().stream().map(UserRoles::name).collect(Collectors.toSet());
            final JwtClaimsBuilder claimsBuilder = getClaimsBuilder(user.getEmail(), groups);

            return claimsBuilder.jws().keyId(signatureKeyId).sign(readPrivateKey(signatureKeyId));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private static JwtClaimsBuilder getClaimsBuilder(String username, Set<String> groups) {
        long now = currentTimeInSecs();

        return Jwt.claims()
                .issuer(iss)
                .subject(sub)
                .audience(aud)
                .expiresAt(Long.MAX_VALUE)
                .issuedAt(now)
                .claim(JTI, UUID.randomUUID().toString())
                .upn(username)
                .groups(groups);
    }

    public static PrivateKey readPrivateKey(final String pemResName) throws Exception {
        InputStream contentIS = Thread.currentThread().getContextClassLoader().getResourceAsStream(pemResName);
        byte[] tmp = new byte[4096];
        int length = contentIS.read(tmp);
        return decodePrivateKey(new String(tmp, 0, length, StandardCharsets.UTF_8));
    }

    public static PrivateKey decodePrivateKey(final String pemEncoded) throws Exception {
        byte[] encodedBytes = toEncodedBytes(pemEncoded);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    private static byte[] toEncodedBytes(final String pemEncoded) {
        final String normalizedPem = removeBeginEnd(pemEncoded);
        return Base64.getDecoder().decode(normalizedPem);
    }

    private static String removeBeginEnd(String pem) {
        pem = pem.replaceAll("-----BEGIN (.*)-----", "");
        pem = pem.replaceAll("-----END (.*)----", "");
        pem = pem.replaceAll("\r\n", "");
        pem = pem.replaceAll("\n", "");
        return pem.trim();
    }

    public static int currentTimeInSecs() {
        long currentTimeMS = System.currentTimeMillis();
        return (int) (currentTimeMS / 1000);
    }
}
