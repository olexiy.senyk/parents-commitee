package org.soo.parent.committee.gate.auth;

import org.soo.common.api.FacadeAccessor;
import org.soo.parent.committee.middleware.security.api.AuthenticationRequest;
import org.soo.parent.committee.middleware.security.api.AuthenticationResponse;
import org.soo.parent.committee.middleware.security.api.AuthenticationResult;
import org.soo.parent.committee.middleware.security.api.AuthenticationService;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/auth")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationResource {

    private static final String AUTHENTICATION_SERVICE_JNDI_NAME = "java:global/middleware/AuthenticationServiceImpl";

    @POST
    @PermitAll
    public AuthenticationResponse authenticate(AuthenticationRequest request) throws NamingException {
        final AuthenticationService authenticationService = FacadeAccessor.lookupService(AUTHENTICATION_SERVICE_JNDI_NAME);
        final AuthenticationResult authenticationResult = authenticationService.authenticate(request);

        return authenticationResult.isAuthenticated()
                ? AuthenticationResponse.authenticated(JwtUtils.createJWT(authenticationResult.getUser()))
                : AuthenticationResponse.notAuthenticated();
    }
}
