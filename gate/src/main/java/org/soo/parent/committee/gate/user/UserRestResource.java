package org.soo.parent.committee.gate.user;

import org.soo.common.api.FacadeAccessor;
import org.soo.parent.committee.middleware.user.api.*;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.naming.NamingException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/user")
@DenyAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserRestResource {

    private static final String USER_SERVICE_JNDI_NAME = "java:global/middleware/UserServiceImpl";

    private static UserService getFacade() throws NamingException {
        return FacadeAccessor.lookupService(USER_SERVICE_JNDI_NAME);
    }

    @POST
    @Path("/registration")
    @PermitAll
    public RegistrationResponse registration(RegistrationRequest request) throws NamingException {
        try {
            request.setRoles(UserRoles.defaultUserRoles());
            return getFacade().registration(request);
        } catch (Exception e) {
            throw e;
        }
    }

    @GET
    @Path("/{id}")
    @RolesAllowed("admin")
    public UserDto findById(@NotNull @PathParam("id") Long id) throws NamingException {
        return getFacade().findById(id);
    }

    @GET
    @Path("/search/{email}")
    @RolesAllowed("admin")
    public UserDto findById(@NotNull @PathParam("email") String email) throws NamingException {
        return getFacade().findByEmail(email);
    }
}
