package org.soo.parent.committee.gate;

import org.eclipse.microprofile.auth.LoginConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
@LoginConfig(authMethod = "MP-JWT", realmName = "jwt-security-realm")
public class GateApplication extends Application {
}
